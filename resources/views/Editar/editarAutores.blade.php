<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca da Lara</title>
        <style>
            html {
                background-color: #00539F;
            }

            body {
                width: 600px;
                margin: 0 auto;
                background-color: #FF9500;
                padding: 0 20px 20px 20px;
                border: 5px solid black;
            } 

            h1 {
                margin: auto;
                margin-top:20px;
                margin-bottom:30px;
                padding: auto;
                color: #00539F;
                text-shadow: 1px 1px 1px black;
            }

            table{
                border-radius: 10px;
                border: black;
            }

            button{
                background-color:#FFE4C4;                
            }

            .tab{
                background-color: white;
            }

            .tab1{
                background-color: #FF4500;
            }

            .hehe{
                background-color: #FF4500;
                border: 1px solid black;
                width: 400px;
                margin:auto;
                
            }

            nav{
                background-color: #FF8C00;
                border-bottom: 3px 	rgb(218,165,32);
                height: 40px;
            }

            .menu{ 
                list-style: none;
                margin-top: 0px;
                margin-left:0px;
                margin-right:0px;
                margin-bottom:10px;
            }

            .item{
                margin-top: 10px;
                display: inline-block;
                margin-left: 45px;
                margin-right: 45px;
            }

            .item a{
                color: floralwhite;
                text-decoration: none;
            }

            .item a:hover {
                color: #D2691E;
                -webkit-text-stroke-width: .3px;
                -webkit-text-stroke-color: #000;
            }
        </style>
        
    </head>
    <body>
    <nav>
        <ul style = "text-align:center" class="menu">
            <li class="item"><a href="/">Home</a></li>
            <li class="item"><a href="/biblioteca">Livros</a></li>
            <li class="item"><a href="/autores">Autores</a></li>
            <li class="item"><a href="/editora">Editoras</a></li>
        </ul>
    </nav>
    <nav>
        <ul style = "text-align:center" class="menu">
           <li class="item"><a href="/editarlivros">Editar livros</a></li>
           <li class="item"><a href="/editarautores">Editar autores</a></li>
            <li class="item"><a href="/editareditoras">Editar editoras</a></li>  
        </ul>
     </nav>
     <hr color="black">
        <div border='4'>
            <h1  style = "text-align:center">Listagem das Autores</h1>
            <table border='1' width="500" align="center">
                <tr>
                    <td  class="tab1" width="500">
                        <h4>ID do Autor:</h4>
                    </td>
                    <td  class="tab1" width="500">
                        <h4>Nome do Autor:</h4>
                    </td>
                    <td  class="tab1" width="500">
                        <h4>Ações</h4>
                    </td>
                </tr>
                @foreach($autor as $autor)
                <tr>
                    <td  class="tab" width="500">
                        {{$autor->id}}
                    </td>
                    <td  class="tab" width="500">
                        {{$autor->autor}}
                    </td>
                    <td  class="tab" width="500">
                        <button id="btneditar">Editar</button> <button id="btndeletar">Deletar</button>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <center>
            <h4>Site desenvolvido por Lara Borges:2021</h4>
        </center>
    </body>
</html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca - Lara Borges</title>
        <style type="text/css">
            html {
                background-color: #00539F;
            }

            body {
                width: 600px;
                margin: 0 auto;
                background-color: #FF9500;
                padding: 0 20px 20px 20px;
                border: 5px solid black;
            } 

            h1 {
                padding: 30px 0px 0px 0px;
                width: 600px;
                margin: auto;
                padding: auto;
                color: #00539F;
                text-shadow: 1px 1px 1px black;
                margin-bottom: 10px;
            }

            h4 {
                padding: 30px 20px 10px 0px;
                width: 600px;
                margin: auto;
                padding: auto;
                color: solid black;
                text-shadow: 3px 3px 3px #CD853F; 
                font-variant: small-caps;
            }

            button{
                background-color:#FFE4C4;
                display: block;
                margin-left: auto;
                margin-right: auto;
                margin-top:10px;                
            }

            .sla{
                text-align: center;
                border: 1px #DAA520;
            }

            nav{
                background-color: #FF8C00;
                border-bottom: 3px 	rgb(218,165,32);
                height: 40px;
            }

            .menu{ 
                list-style: none;
                margin-top: 0px;
                margin-left:0px;
                margin-right:0px;
            }

            .item{
                margin-top: 10px;
                display: inline-block;
                margin-left: 45px;
                margin-right: 45px;
            }

            .item a{
                color: floralwhite;
                text-decoration: none;
            }

            .item a:hover {
                color: #D2691E;
                -webkit-text-stroke-width: .3px;
                -webkit-text-stroke-color: #000;
            }

        </style>
    </head>
    <body>
        <nav>
            <ul style = "text-align:center" class="menu">
                <li class="item"><a href="/">Home</a></li>
                <li class="item"><a href="/biblioteca">Livros</a></li>
                <li class="item"><a href="/autores">Autores</a></li>
                <li class="item"><a href="/editora">Editoras</a></li>
            </ul>
        </nav>
        <nav>
            <ul style = "text-align:center" class="menu">
                <li class="item"><a href="/editarlivros">Editar livros</a></li>
                <li class="item"><a href="/editarautores">Editar autores</a></li>
                <li class="item"><a href="/editareditoras">Editar editoras</a></li>  
            </ul>
        </nav>
        <hr color="black">
        <h1 style = "text-align:center">Pagina Inicial</h1>
        <div>
            <h4  style = "text-align:center" >Clique para ter acesso a Biblioteca</h4>
        </div class="sla">
        <a href="/biblioteca"> 
        <button>Biblioteca</button></a>
    </body>
</html>

